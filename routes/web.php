<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'APIController@get');
Route::get('/api/btg', 'APIController@btg');
Route::get('/api', 'RPCController@get');

Route::get('/set_fanspeed/', 'APIController@setFanSpeed');
Route::get('/set_gpu_clock/', 'APIController@setGPUClock');
Route::get('/set_mem_clock/', 'APIController@setMemClock');
Route::get('/set_tdp/', 'APIController@setTDP');
Route::get('/check/{a}', 'APIController@checkStatus');
Route::get('/stop/{a}','APIController@stopMiner');

Route::get('/run_eth', function(){
	$args = "/home/reiben/ethminer --farm-recheck 200 -U -S asia1.ethermine.org:14444 -O 0x9fc2244a49d78d4de574481fa42b5f3833755287.".env('WORKER_NAME')." --api-port -3333 --cuda-devices ".env('CUDA_DEVICES');
	print_r($args);
	shell_exec($args);
});

Route::get('/run_btg', function(){
	$args = "/home/reiben/miner --server btg.suprnova.cc --port 8866 --user reiben05.".env('WORKER_NAME')." --pass password --algo 144_5 --pers BgoldPoW --api 0.0.0.0:42000  --cuda_devices ".env('CUDA_DEVICES');
	//return $args; exit;
	print_r($args);
	shell_exec($args);
});

