<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Crypt;
use Session;
use Cookie;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;


class APIController extends Controller
{

    protected function get(Request $request){

        $args = $request->input('args');

        $cpu = $this->get_server_cpu_usage();
        $ram = $this->get_server_memory_usage();
        $temperature = $this->getSensors();

        //print_r('<pre>');print_r($temperature);exit;
        //return response()->json(['temperature'=>$temperature]);
        


        if(empty($args)){
		  $args = 'nvidia-smi --query-gpu=pci.bus_id,name,power.draw,fan.speed,temperature.gpu,clocks.gr,clocks.mem,utilization.memory --format=csv,noheader';  	
	    }

        $data = null;
        $storage = $this->shapeSpace_disk_usage();

        $process = new Process($args);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            //throw new ProcessFailedException($process);
        	//$a = new ProcessFailedException($process);
            $code = 404;
            $data = $process->getOutput();
        }
        else{
            $data = $process->getOutput();
            $code = 200;
        }
    
        return response()->json(['data'=>$data, 'temperature'=>$temperature,'code'=>$code, 'storage'=>$storage, 'cpu'=>$cpu, 'ram'=>$ram]);
    }

    protected function btg(Request $request){
        $data = null;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://0.0.0.0:42000/getstat');
        curl_setopt($ch, CURLOPT_TIMEOUT, 700);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
        $response = curl_exec($ch);
        $data = json_decode($response);

        return response()->json(['data'=>$data]);
    }

    protected function get_server_memory_usage(){
        $free = shell_exec('free -m');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_usage = $mem[2]/$mem[1]*100;
        
        $arr = [$mem[1],$mem[2]];
        return $arr;
    }

    protected function get_server_cpu_usage(){

        $load = sys_getloadavg();
        return $load[0];
    }

    protected function shapeSpace_disk_usage() {
    
        $disktotal = disk_total_space ('/');
        $diskfree  = disk_free_space  ('/');
        //$diskuse   = round (100 - (($diskfree / $disktotal) * 100)) .'%';
        

        return [$disktotal,$diskfree];
    }


    protected function getSensors() {
        $sensors = shell_exec('sensors');
        //$sensors = preg_replace('/\s+/', '', $sensors);

        //return $sensors;

        //$sens = explode('Core',$sensors);
        //return sizeof($sens);

        return $sensors;
    }

    protected function setFanSpeed(Request $request){
        $msg = null;

        $fanspeed = $request->input('fan_speed');
        $fanspeed = preg_replace('/[^0-9]/', '', $fanspeed);
        $gpu_id = null;

        if(!isset($fanspeed) ){
            $msg = 'Empty fanspeed';
        }
        else{
            if($fanspeed>100){
                $msg = 'Fan speed cannot be more than 100%!';
            }
            else if($fanspeed<10){
                $msg = 'Fan speed cannot be less than 10%!';
            }
            else{
                $gpu_id = $request->input('gpu_id');

                //$gpu_id = preg_replace('/-[^0-9]/', '', $gpu_id);

                if(isset($gpu_id)){
                    $args1 = '';
                    $args1 .= 'sudo DISPLAY=:0 XAUTHORITY=/var/run/lightdm/root/:0 nvidia-settings -a [gpu:'.$gpu_id.']/GPUFanControlState=1 2>&1;';
                    $args1 .= 'sudo DISPLAY=:0 XAUTHORITY=/var/run/lightdm/root/:0 nvidia-settings -a [fan:'.$gpu_id.']/GPUTargetFanSpeed='.$fanspeed.' 2>&1;';
                    
                    $msg = shell_exec($args1);
                }
                else{
                    $msg ='Missing gpu id!';
                }
            }
        }
        return response()->json(['fanspeed'=>$fanspeed, 'msg'=>$msg, 'gpu_id'=>$gpu_id]);
    }

    protected function setGPUClock(Request $request){
        $msg = null;

        $gpu_id = null;
        $gpu_clock = $request->input('gpu_clock');

        if(!isset($gpu_clock) ){
            $msg = 'Empty gpu clock.';
        }
        else{
            if($gpu_clock>250){
                $msg = 'GPU clock cannot be more than 250!';
            }
            else if($gpu_clock<-250){
                $msg = 'GPU clock cannot be less than -250';
            }
            else{
                $gpu_id = $request->input('gpu_id');

                $perf_id = $request->input('perf_id');

                if(isset($gpu_id) && isset($perf_id)){
                    $args1 = '';
                    $args1 .= 'sudo DISPLAY=:0 XAUTHORITY=/var/run/lightdm/root/:0 nvidia-settings -a [gpu:'.$gpu_id.']/GPUGraphicsClockOffset['.$perf_id.']='.$gpu_clock.' 2>&1;';
                    
                    $msg = shell_exec($args1);
                }
                else{
                    $msg ='Missing gpu id!';
                }
            }
        }
        return response()->json(['gpu_clock'=>$gpu_clock, 'msg'=>$msg, 'gpu_id'=>$gpu_id]);
    }

    protected function setMemClock(Request $request){
        $msg = null;

        $gpu_id = null;
        $mem_clock = $request->input('mem_clock');

        if(!isset($mem_clock) ){
            $msg = 'Empty gpu clock.';
        }
        else{
            if($mem_clock>1500){
                $msg = 'Mem clock cannot be more than 1500!';
            }
            else if($mem_clock<-1000){
                $msg = 'Mem clock cannot be less than -1000';
            }
            else{
                $gpu_id = $request->input('gpu_id');

                $perf_id = $request->input('perf_id');

                if(isset($gpu_id) && isset($perf_id)){
                    $args1 = '';
                    $args1 .= 'sudo DISPLAY=:0 XAUTHORITY=/var/run/lightdm/root/:0 nvidia-settings -a [gpu:'.$gpu_id.']/GPUMemoryTransferRateOffset['.$perf_id.']='.$mem_clock.' 2>&1;';
                    
                    $msg = shell_exec($args1);
                }
                else{
                    $msg ='Missing gpu id!';
                }
            }
        }
        return response()->json(['mem_clock'=>$mem_clock, 'msg'=>$msg, 'gpu_id'=>$gpu_id]);
    }

    protected function setTDP(Request $request){
        $msg = null;

        $gpu_id = null;
        $tdp = $request->input('tdp');

        if(!isset($tdp)){
            $msg = 'Empty TDP.';
        }
        else{
            $gpu_id = $request->input('gpu_id');

            if(isset($gpu_id)){
                $args1 = '';
                $args1 .= 'sudo nvidia-smi -i '.$gpu_id.' -pl '.$tdp.' 2>&1';
                
                $msg = shell_exec($args1);
            }
            else{
                $msg ='Missing gpu id!';
            }
        
        }
        return response()->json(['tdp'=>$tdp, 'msg'=>$msg, 'gpu_id'=>$gpu_id]);
    }

    protected function checkStatus(Request $request,$a){
        if(empty($a)){
            $msg = 'Missing argument!';
        }
        else{
            $args1='pidof '.$a.' 2>&1';
            $dd1 = shell_exec($args1);
            
            if(!empty($dd1)){
                $msg = ucwords($a).' is running with PID of '.$dd1;

            }
            else{
                $msg = 'No process found.';
            }
        }

        return response()->json(['args'=>$a, 'msg'=>$msg]);
    }

    protected function stopMiner(Request $request,$a){
        $msg = null;

        if(empty($a)){
            $msg = 'Missing argument!';
        }
        else{
            if($a=='eth'){
                $args1 = 'pkill -9 ethminer 2>&1';
                $dd1 = shell_exec($args1);

                if(!empty($dd1)){
                    $msg = 'Ethminer successfully terminated.';
                }
                else{
                    $msg = 'No ETH process found.';
                }
            }
            else if($a=='btg'){
                $args1 = 'pkill -9 miner 2>&1';
                $dd1 = shell_exec($args1);

                if(!empty($dd1)){
                    $msg = 'EWBF miner successfully terminated.';
                }
                else{
                    $msg = 'No BTG process found.';
                }
            }
            else{
                $msg = 'No coins found.';
            }
        }

        return response()->json(['args'=>$a, 'msg'=>$msg]);
    }




}
